declare function require(name:string);

require('./index.html');

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as Redux from 'redux';
import * as ReactRedux from 'react-redux'; 

import app from "./reducers";

import {Hello} from './components/Hello';

let store = Redux.createStore(app)

ReactDOM.render(
    <ReactRedux.Provider store={store}>
        <div>
            <Hello firstname="a" lastname="b"/>
        </div>
    </ReactRedux.Provider> 
    , document.getElementById('content')
);

