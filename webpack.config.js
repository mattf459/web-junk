module.exports = {
    entry: './index.tsx',
    output: {
        filename: 'bundle.js',
        path: __dirname + '/build',
        publicPath: 'http://localhost:8090/assets'
    },
    module: {
        loaders: [
            {
                test: /\.jsx$/,
                loader: 'jsx-loader',
                exclude: /node_modules/
            },
            {
                test: /\.tsx$/,
                loader: 'ts-loader!ts-jsx-loader',
                exclude: /node_modules/
            },
            {
                test: /\.(html)$/, 
                loader: 'file-loader?name=[name].[ext]'
            }
        ]
    },
    resolve: {
        extensions: ['', '.js', '.jsx', '.ts', '.tsx', '.html']
    }
}
